#!/usr/bin/python3

import asyncio
import os
import pathlib
import re
import sys

import aiofiles
import frontmatter
import httpx

import utils


def generate_filename(item):
    if app_id := utils.get_recursive(item, "extra.app_id"):
        return app_id.lower() + ".md"

    m = re.match(r"^(?:https?://)(?P<domain>[^/]+)/(?P<path>.*)$", item["extra"]["repository"]).groupdict()
    path_name = ".".join([name.lower().replace("~", "").replace("_", "-") for name in m["path"].split("/") if name])
    return f"noappid.{path_name}.md"


async def check_file(client, filename, update=False):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.loads(await f.read())

    generated_filename = filename.parent / generate_filename(doc.metadata)
    if filename == generated_filename:
        return False

    print(f"File name should be {generated_filename} instead of {filename}", file=sys.stderr)
    if update:
        print(f"Renaming {filename} to {generated_filename}")
        os.rename(filename, generated_filename)

    return True


async def run(folder, update=False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        tasks = []
        for filename in folder.glob("**/*.md"):
            if filename.name == "_index.md":
                continue
            tasks.append(asyncio.ensure_future(check_file(client, filename, update)))
        found = any(await asyncio.gather(*tasks))
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix FOLDER")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_folder = pathlib.Path(sys.argv[2])
    found = await run(apps_folder, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_folder}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
