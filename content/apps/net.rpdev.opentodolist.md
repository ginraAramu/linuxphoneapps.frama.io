+++
title = "OpenTodoList"
description = "A Todo List and Note Taking Application."
aliases = []
date = 2021-06-15
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Martin Höher",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://gitlab.com/rpdev/opentodolist"
homepage = "https://opentodolist.rpdev.net/"
bugtracker = "https://gitlab.com/rpdev/opentodolist/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://opentodolist.rpdev.net/"
screenshots = [ "https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/173",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "net.rpdev.OpenTodoList"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.rpdev.OpenTodoList"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "opentodolist",]
appstream_xml_url = "https://gitlab.com/rpdev/opentodolist/-/raw/development/net.rpdev.OpenTodoList.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++



### Description

OpenTodoList is a todo list and note taking application. Organize todo lists, notes and images in libraries, which can be stored either completely local on the device you are working on (and hence ensure no information leaks out to untrusted third parties) or use the built in synchronization features which allows you to synchronize your libraries across devices using your self-hosted NextCloud or ownCloud server (or other WebDAV servers). In addition, a library is just a directory holding the items of your library as simple files - this allows you to use any kind of third party synchronization tool (like DropBox) to sync your information. [Source](https://gitlab.com/rpdev/opentodolist)
