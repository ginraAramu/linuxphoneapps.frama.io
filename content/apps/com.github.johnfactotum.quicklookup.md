+++
title = "Quick Lookup"
description = "Quick Lookup is a simple GTK dictionary application powered by Wiktionary"
aliases = []
date = 2020-10-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "John Factotum",]
categories = [ "dictionary",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = [ "Wiktionary",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "TextTools", "Dictionary",]
programming_languages = [ "JavaScript",]
build_systems = [ "custom",]

[extra]
repository = "https://github.com/johnfactotum/quick-lookup"
homepage = ""
bugtracker = "https://github.com/johnfactotum/quick-lookup/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/johnfactotum/quick-lookup"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.johnfactotum.QuickLookup"
scale_to_fit = "gjs"
flathub = "https://flathub.org/apps/com.github.johnfactotum.QuickLookup"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "quick-lookup",]
appstream_xml_url = "https://raw.githubusercontent.com/johnfactotum/quick-lookup/master/com.github.johnfactotum.QuickLookup.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Notice

Great after scale to fit
