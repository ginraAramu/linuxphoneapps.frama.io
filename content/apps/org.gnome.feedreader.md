+++
title = "FeedReader"
description = "FeedReader is a modern desktop application designed to complement existing web-based RSS accounts."
aliases = []
date = 2020-10-15
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jan Lukas Gernert",]
categories = [ "feed reader",]
mobile_compatibility = [ "2",]
status = [ "archived",]
frameworks = [ "GTK3",]
backends = []
services = [ "TT-RSS", "Feedly", "Feedin", "Fresh Rss", "Inoreader", "The Old Reader", "Nextcloud", "RSS",]
packaged_in = [ "aur", "debian_11", "debian_unstable", "devuan_4_0", "pureos_landing",]
freedesktop_categories = [ "GTK", "Network", "Feed",]
programming_languages = [ "Vala", "C",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/jangernert/FeedReader"
homepage = "https://jangernert.github.io/FeedReader/"
bugtracker = "https://github.com/jangernert/FeedReader/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/jangernert/FeedReader/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.feedreader"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "feedreader",]
appstream_xml_url = "https://raw.githubusercontent.com/jangernert/FeedReader/master/data/org.gnome.FeedReader.appdata.xml.in"
reported_by = "ula"
updated_by = "script"
+++




### Notice

Not maintained any more, the developer is now making NewsFlash. Scale-to-fit necessary and even with the last pane of is not completely displayed and some crash, Launch and configuration works. Archived as the project has been archived. It has been continued as Communique for Elementary OS, but as that does not target mobile devices, Communique has not been added.