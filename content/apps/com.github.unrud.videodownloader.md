+++
title = "Video Downloader"
description = "Download videos from websites like YouTube and many others (based on youtube-dl)"
aliases = []
date = 2020-10-15
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Unrud",]
categories = [ "video downloader",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_rawhide", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/Unrud/video-downloader"
homepage = ""
bugtracker = "https://github.com/Unrud/video-downloader/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Unrud/video-downloader"
screenshots = [ "https://fosstodon.org/web/statuses/105069105324161487",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.unrud.VideoDownloader"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.unrud.VideoDownloader"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "video-downloader",]
appstream_xml_url = "https://raw.githubusercontent.com/Unrud/video-downloader/master/data/com.github.unrud.VideoDownloader.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Download videos from websites with an easy-to-use interface. Provides the following features:      Convert videos to MP3     Supports password-protected and private videos     Download single videos or whole playlists     Automatically selects a video format based on your quality demands [Source](https://github.com/Unrud/video-downloader)
