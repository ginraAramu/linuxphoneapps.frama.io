+++
title = "Warpinator"
description = "Share files across the LAN"
aliases = []
date = 2022-03-12
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Linux Mint",]
categories = [ "file transfer",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "xapps",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "Network", "FileTransfer",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/linuxmint/warpinator"
homepage = ""
bugtracker = "https://github.com/linuxmint/warpinator/issues/"
donations = "https://www.linuxmint.com/donors.php"
translations = "https://translations.launchpad.net/linuxmint/latest/+pots/warpinator"
more_information = []
summary_source_url = "https://github.com/linuxmint/warpinator"
screenshots = [ "https://nitter.net/pic/media%2FFNpTKFvXoAgXcNJ.jpg",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.x.Warpinator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.x.Warpinator"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "warpinator",]
appstream_xml_url = "https://raw.githubusercontent.com/linuxmint/warpinator/master/data/org.x.Warpinator.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Warpinator allows you to easily connect multiple computers on a local area network and share files quickly and securely. [Source](https://flathub.org/apps/org.x.Warpinator)


### Notice

While some elements of the screen may be cut off, it generally scales extremely well. With flatpak, the file picker dialog is not fitting well, but hitting enter can solve this.
