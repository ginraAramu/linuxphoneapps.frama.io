+++
title = "phom"
description = "Quick hack to have a virtual mouse"
aliases = []
date = 2020-11-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "guido.gunther",]
categories = [ "virtual input device",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://source.puri.sm/guido.gunther/phom"
homepage = ""
bugtracker = "https://source.puri.sm/guido.gunther/phom/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/guido.gunther/phom"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++