+++
title = "WhatIP"
description = "Info on your IP"
aliases = []
date = 2020-08-26
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gabriele Musco",]
categories = [ "network",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_rawhide", "flathub", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "Network", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GabMus/whatip"
homepage = "https://whatip.gabmus.org/"
bugtracker = "https://gitlab.gnome.org/GabMus/whatip/-/issues/"
donations = "https://liberapay.com/gabmus/donate"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GabMus/whatip"
screenshots = [ "https://wiki.mobian-project.org/doku.php?id=whatip",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gabmus.whatip"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gabmus.whatip"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "whatip",]
appstream_xml_url = "https://gitlab.gnome.org/GabMus/whatip/-/raw/master/data/org.gabmus.whatip.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Get your IP easily.  Be it local, public or a virtual interfaces, it’s easy to understand and one click away. [Source](https://gabmus.gitlab.io/whatip/)
