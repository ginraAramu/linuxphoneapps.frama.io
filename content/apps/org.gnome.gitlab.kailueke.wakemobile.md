+++
title = "wake-mobile"
description = "Proof-of-concept alarm app that uses systemd timers to wake up the system"
aliases = []
date = 2021-03-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "alarm clock",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Utility", "Clock",]
programming_languages = [ "Python", "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/kailueke/wake-mobile"
homepage = ""
bugtracker = "https://gitlab.gnome.org/kailueke/wake-mobile/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/kailueke/wake-mobile"
screenshots = [ "https://gitlab.gnome.org/kailueke/wake-mobile",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.gitlab.kailueke.WakeMobile"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "wake-mobile",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++




### Notice

Just really simple, working Proof-of-Concept, easy to make install and uninstall