+++
title = "SpaceBar"
description = "SMS/MMS application for Plasma Mobile"
aliases = []
date = 2019-09-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "SMS", "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = [ "ofono",]
services = [ "SMS", "MMS",]
packaged_in = [ "alpine_3_17", "alpine_edge", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Chat",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/plasma-mobile/spacebar"
homepage = ""
bugtracker = "https://invent.kde.org/plasma-mobile/spacebar/-/issues/"
donations = ""
translations = ""
more_information = [ "https://phabricator.kde.org/project/profile/66/", "https://phabricator.kde.org/T6936", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#spacebar",]
summary_source_url = "https://invent.kde.org/plasma-mobile/spacebar"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.spacebar"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "spacebar",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/spacebar/-/raw/master/org.kde.spacebar.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++



### Description

Spacebar is an SMS/MMS application that primarily targets Plasma Mobile. It depends on Qt and few KDE Frameworks (Kirigami2, KI18n, KPeople, and ModemManagerQt). [Source](https://invent.kde.org/plasma-mobile/spacebar)
