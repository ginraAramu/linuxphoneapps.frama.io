+++
title = "Idok Remote"
description = "A simple Kodi remote that supports multiple Kodi instances."
aliases = []
date = 2021-03-10
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "the3dman",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = [ "Kodi",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Network", "RemoteAccess",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "qmake",]

[extra]
repository = "https://gitlab.com/The3DmaN/idokremote"
homepage = "https://the3dman.gitlab.io/"
bugtracker = "https://gitlab.com/The3DmaN/idokremote/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/The3DmaN/idokremote"
screenshots = [ "https://gitlab.com/The3DmaN/idokremote",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "idokremote",]
appstream_xml_url = ""
reported_by = "The3DmaN"
updated_by = "script"
+++

