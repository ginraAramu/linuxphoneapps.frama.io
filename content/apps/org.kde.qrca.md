+++
title = "Qrca"
description = "QR code scanner for Plasma Mobile"
aliases = []
date = 2019-09-30
updated = 2023-02-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "qr code",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = [ "gstreamer", "zxing-cpp",]
services = []
packaged_in = [ "alpine_3_17", "alpine_edge",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/qrca"
homepage = ""
bugtracker = "https://invent.kde.org/utilities/qrca/-/issues/"
donations = ""
translations = ""
more_information = [ "https://phabricator.kde.org/T8906",]
summary_source_url = "https://invent.kde.org/utilities/qrca"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.qrca"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "qrca",]
appstream_xml_url = "https://invent.kde.org/utilities/qrca/-/raw/master/org.kde.qrca.appdata.xml"
reported_by = "cahfofpai"
updated_by = "linmob"
+++



### Description

Scan QR-Codes with your camera on phones and laptops, and create your own for easily sharing data between devices. [Source](https://invent.kde.org/utilities/qrca/-/raw/master/org.kde.qrca.appdata.xml)
