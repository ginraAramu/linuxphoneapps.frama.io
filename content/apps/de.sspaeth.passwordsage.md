+++
title = "PasswordSage"
description = "A password manager geared towards mobile screens. It uses GTK as framework and parses .kdbx files (version 3.1 and 4)."
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "sspaeth",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "pykeepass",]
services = [ "KeePass",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/sspaeth/passwordsage"
homepage = ""
bugtracker = "https://gitlab.com/sspaeth/passwordsage/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/sspaeth/passwordsage"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "de.sspaeth.passwordsage"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/sspaeth/passwordsage/-/raw/master/data/de.sspaeth.passwordsage.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Last commit in August 2020