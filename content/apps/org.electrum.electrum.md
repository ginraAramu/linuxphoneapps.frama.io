+++
title = "Electrum"
description = "Lightweight Bitcoin client"
aliases = []
date = 2020-10-21
updated = 2023-03-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "spesmilo",]
categories = [ "bitcoin wallet",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtWidgets", "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://github.com/spesmilo/electrum"
homepage = "https://electrum.org"
bugtracker = "https://github.com/spesmilo/electrum/issues/"
donations = ""
translations = ""
more_information = [ "https://github.com/spesmilo/electrum/issues/6835",]
summary_source_url = "https://flathub.org/apps/org.electrum.electrum"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.electrum.electrum"
scale_to_fit = "electrum"
flathub = "https://flathub.org/apps/org.electrum.electrum"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "electrum",]
appstream_xml_url = "https://raw.githubusercontent.com/spesmilo/electrum/master/org.electrum.electrum.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"
+++




### Notice

Ok after scale-to-fit. The app has a Kivy GUI for Android, which should do better - but needs custom building. If you manage to run the Kivy GUI on mobile GNU/Linux, please report back :-)
