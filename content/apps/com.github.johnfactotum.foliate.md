+++
title = "Foliate"
description = "A simple and modern eBook viewer"
aliases = []
date = 2020-10-08
updated = "2023-04-15"

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "John Factotum",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "epub.js",]
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Office", "Viewer",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/johnfactotum/foliate"
homepage = "https://johnfactotum.github.io/foliate/"
bugtracker = "https://github.com/johnfactotum/foliate/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/johnfactotum/foliate"
screenshots = [ "https://twitter.com/linmobblog/status/1314079080145448961#m", "https://raw.githubusercontent.com/johnfactotum/foliate/master/data/screenshots/view.png", "https://raw.githubusercontent.com/johnfactotum/foliate/master/data/screenshots/dark.png", "https://raw.githubusercontent.com/johnfactotum/foliate/master/data/screenshots/skeuomorphism.png", "https://raw.githubusercontent.com/johnfactotum/foliate/master/data/screenshots/note.png", "https://raw.githubusercontent.com/johnfactotum/foliate/master/data/screenshots/annotations.png", "https://raw.githubusercontent.com/johnfactotum/foliate/master/data/screenshots/lookup.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.johnfactotum.foliate/1.png", "https://img.linuxphoneapps.org/com.github.johnfactotum.foliate/2.png", "https://img.linuxphoneapps.org/com.github.johnfactotum.foliate/3.png", "https://img.linuxphoneapps.org/com.github.johnfactotum.foliate/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.johnfactotum.Foliate"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.johnfactotum.Foliate"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "foliate",]
appstream_xml_url = "https://raw.githubusercontent.com/johnfactotum/foliate/master/data/com.github.johnfactotum.Foliate.metainfo.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++


### Description

Foliate is a simple and modern GTK eBook viewer.

Features include:

*   Two-page view and scrolled view
*   Customize font and line-spacing
*   Light, sepia, dark, and invert mode
*   Reading progress slider with chapter marks
*   Bookmarks and annotations
*   Find in book
*   Quick dictionary lookup

[Source](https://raw.githubusercontent.com/johnfactotum/foliate/master/data/com.github.johnfactotum.Foliate.metainfo.xml.in)
