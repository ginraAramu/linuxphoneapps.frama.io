+++
title = "Buho"
description = "Maui Note Taking App"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "maui",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "Utility", "TextTools",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/maui/buho"
homepage = "https://mauikit.org/apps/buho/"
bugtracker = "https://invent.kde.org/maui/buho/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/buho"
screenshots = [ "https://medium.com/@temisclopeolimac/mauis-monthly-c39bbb2de9d0", "https://medium.com/nitrux/maui-apps-apk-packages-5c966f185f0c", "https://medium.com/nitrux/maui-kde-fcdc920138e2", "https://medium.com/nitrux/maui-plasma-mobile-sprint-2019-c20031700b3b",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.buho"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "buho",]
appstream_xml_url = "https://invent.kde.org/maui/buho/-/raw/master/src/org.kde.buho.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++



### Description

Buho allows you to save links, write quick notes and organize pages as books. Buho works on desktops, Android and Plasma Mobile. [Source](https://invent.kde.org/maui/buho)
