+++
title = "Flatseal"
description = "Manage Flatpak permissions"
aliases = []
date = 2020-08-26
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Martin Abente Lahaye",]
categories = [ "settings",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_rawhide", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/tchx84/flatseal"
homepage = ""
bugtracker = "https://github.com/tchx84/flatseal/issues/"
donations = ""
translations = ""
more_information = [ "https://blogs.gnome.org/tchx84/2022/05/31/flatseal-1-8-0/",]
summary_source_url = "https://github.com/tchx84/flatseal"
screenshots = [ "https://wiki.mobian-project.org/doku.php?id=flatseal",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.tchx84.Flatseal"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.tchx84.Flatseal"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "flatseal",]
appstream_xml_url = "https://raw.githubusercontent.com/tchx84/Flatseal/master/data/com.github.tchx84.Flatseal.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Flatseal is a graphical utility to review and modify basic permissions from your Flatpak applications. [Source](https://github.com/tchx84/flatseal)


### Notice

If you use any Flatpaks, use this app, too.
