+++
title = "Home Assistant Connect (HAC)"
description = "HA Connect is a simple web app for connecting to the web interface of an Home Assistant server."
aliases = []
date = 2021-01-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "the3dman",]
categories = [ "smart home",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = [ "Home Assistant",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Network", "RemoteAccess",]
programming_languages = [ "QML", "Shell", "Cpp",]
build_systems = [ "qmake",]

[extra]
repository = "https://gitlab.com/The3DmaN/ha-connect"
homepage = ""
bugtracker = "https://gitlab.com/The3DmaN/ha-connect/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/The3DmaN/ha-connect"
screenshots = [ "https://gitlab.com/The3DmaN/ha-connect",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "ha-connect",]
appstream_xml_url = ""
reported_by = "The3DmaN"
updated_by = "script"
+++

