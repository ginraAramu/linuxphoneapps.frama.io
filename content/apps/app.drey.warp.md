+++
title = "Warp"
description = "Fast and secure file transfer"
aliases = []
date = 2022-05-21
updated = 2022-06-29

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Fina Wilke",]
categories = [ "file transfer",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Magic Wormhole",]
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "Network", "FileTransfer",]
programming_languages = [ "Rust", "Python",]
build_systems = [ "meson", "cargo",]

[extra]
repository = "https://gitlab.gnome.org/World/warp"
homepage = ""
bugtracker = "https://apps.gnome.org/app/app.drey.Warp/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://apps.gnome.org/app/app.drey.Warp/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "app.drey.Warp"
scale_to_fit = "app.drey.Warp"
flathub = "https://flathub.org/apps/app.drey.Warp"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "warp-share-files",]
appstream_xml_url = "https://gitlab.gnome.org/World/warp/-/raw/main/data/app.drey.Warp.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

Warp allows you to securely send files to each other via the internet or local network by exchanging a word-based code.  The best transfer method will be determined using the 'Magic Wormhole' protocol which includes local network transfer if possible.  Features: - Send files between multiple devices, - Every file transfer is encrypted, - Directly transfer files on the local network if possible, -  An internet connection is required, - Compatibility with the 'Magic Wormhole' command line client [Source](https://apps.gnome.org/app/app.drey.Warp/)


### Notice

Fully adaptive since 0.2.0.
