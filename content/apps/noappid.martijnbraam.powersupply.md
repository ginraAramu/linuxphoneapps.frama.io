+++
title = "Powersupply"
description = "PinePhone power status tool"
aliases = []
date = 2022-03-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "martijnbraam",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_edge", "pureos_landing",]
freedesktop_categories = [ "GTK", "System",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://gitlab.com/MartijnBraam/powersupply"
homepage = ""
bugtracker = "https://gitlab.com/MartijnBraam/powersupply/-/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.postmarketos.org/wiki/Applications_by_category#System",]
summary_source_url = "https://gitlab.com/MartijnBraam/powersupply"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "powersupply",]
appstream_xml_url = "https://gitlab.com/MartijnBraam/powersupply/-/raw/master/data/nl.brixit.powersupply.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++



### Description

GTK3 app to display power status of phones [Source](https://gitlab.com/MartijnBraam/powersupply)