+++
title = "Kasts"
description = "Kirigami-based podcast player"
aliases = []
date = 2021-05-05
updated = 2023-02-14

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The KDE Community",]
categories = [ "podcast client",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Audio", "Feed", "Player",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/multimedia/kasts"
homepage = "https://apps.kde.org/kasts/"
bugtracker = ""
donations = "https://bugs.kde.org/enter_bug.cgi?product=kasts"
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#kasts", "https://plasma-mobile.org/2023/01/30/january-blog-post/#kasts-podcast",]
summary_source_url = "https://invent.kde.org/multimedia/kasts"
screenshots = [ "https://cdn.kde.org/screenshots/kasts/kasts-mobile-player.png", "https://cdn.kde.org/screenshots/kasts/kasts-mobile.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kasts"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kasts"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kasts",]
appstream_xml_url = "https://invent.kde.org/multimedia/kasts/-/raw/master/org.kde.kasts.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

Kasts is a convergent podcast application. [Source](https://invent.kde.org/multimedia/kasts)
