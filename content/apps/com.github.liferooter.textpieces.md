+++
title = "Text Pieces"
description = "Transform text without using random websites"
aliases = []
date = 2022-09-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gleb Smirnov",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "gnuguix", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextTools",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/liferooter/textpieces"
homepage = ""
bugtracker = "https://github.com/liferooter/textpieces/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/com.github.liferooter.textpieces/",]
summary_source_url = "https://raw.githubusercontent.com/liferooter/textpieces/main/data/com.github.liferooter.textpieces.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/liferooter/textpieces/6b766fbb9956175db454c3266ab3d8c2bfa57b07/screenshots/slide1.png", "https://raw.githubusercontent.com/liferooter/textpieces/6b766fbb9956175db454c3266ab3d8c2bfa57b07/screenshots/slide2.png", "https://raw.githubusercontent.com/liferooter/textpieces/6b766fbb9956175db454c3266ab3d8c2bfa57b07/screenshots/slide3.png", "https://raw.githubusercontent.com/liferooter/textpieces/6b766fbb9956175db454c3266ab3d8c2bfa57b07/screenshots/slide4.png", "https://raw.githubusercontent.com/liferooter/textpieces/6b766fbb9956175db454c3266ab3d8c2bfa57b07/screenshots/slide5.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/1.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/2.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/3.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/4.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/5.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/6.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/7.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/8.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.liferooter.textpieces"
scale_to_fit = "com.github.liferooter.textpieces"
flathub = "https://flathub.org/apps/com.github.liferooter.textpieces"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "textpieces",]
appstream_xml_url = "https://raw.githubusercontent.com/liferooter/textpieces/main/data/com.github.liferooter.textpieces.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Do a lot of text transformations, such as: Calculate hashes, Encode text, Decode text, Remove trailing spaces and lines, Count lines, symbols and words, Format JSON and XML, Escape and unescape strings, Convert JSON to YAML and vice versa, Filter lines, Replace substrings and regular expressions ...and so on. [Source](https://raw.githubusercontent.com/liferooter/textpieces/main/data/com.github.liferooter.textpieces.appdata.xml.in)


### Notice

Unofficial [APKBUILD](https://framagit.org/linmobapps/apkbuilds/textpieces/)
