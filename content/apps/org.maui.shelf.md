+++
title = "Shelf"
description = "Maui Library - Document and EBook Collection Manager"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "maui",]
categories = [ "document and ebook manager",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "Office", "Viewer",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/maui/shelf"
homepage = "https://mauikit.org/apps/shelf/"
bugtracker = "https://invent.kde.org/maui/shelf/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/shelf"
screenshots = [ "https://medium.com/nitrux/maui-kde-fcdc920138e2",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.maui.shelf"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "maui-shelf", "shelf",]
appstream_xml_url = "https://invent.kde.org/maui/shelf/-/raw/master/org.kde.shelf.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++

