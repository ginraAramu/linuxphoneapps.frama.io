+++
title = "OTPClient"
description = "Highly secure and easy to use OTP client written in C/GTK that supports both TOTP and HOTP"
aliases = []
date = 2021-07-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-4.0-only",]
app_author = [ "Paolo Stivanin",]
categories = [ "multi-factor authentication",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = [ "libcotp",]
services = []
packaged_in = [ "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "flathub", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/paolostivanin/OTPClient"
homepage = ""
bugtracker = "https://github.com/paolostivanin/OTPClient/issues/"
donations = ""
translations = ""
more_information = [ "https://github.com/paolostivanin/OTPClient/wiki",]
summary_source_url = "https://github.com/paolostivanin/OTPClient"
screenshots = [ "https://pbs.twimg.com/media/E7FA_n2XoAMRq0X?format=jpg&name=large",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.paolostivanin.OTPClient"
scale_to_fit = "otpclient"
flathub = "https://flathub.org/apps/com.github.paolostivanin.OTPClient"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "otpclient",]
appstream_xml_url = "https://raw.githubusercontent.com/paolostivanin/OTPClient/master/data/com.github.paolostivanin.OTPClient.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Highly secure and easy to use GTK+ software for two-factor authentication that supports both Time-based One-time Passwords (TOTP) and HMAC-Based One-Time Passwords (HOTP). [Source](https://github.com/paolostivanin/OTPClient)


### Notice

Supports importing from many other OTP apps.
