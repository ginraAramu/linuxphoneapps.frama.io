+++
title = "GNOME Calls"
description = "A phone dialer and call handler."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "telephony",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = [ "ModemManager", "oFono", "Phonesim", "Sofia-SIP",]
services = [ "SIP",]
packaged_in = [ "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "flathub", "manjaro_stable", "manjaro_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Dialup",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/calls"
homepage = ""
bugtracker = "https://gitlab.gnome.org/GNOME/calls/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/calls"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Calls"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Calls"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-calls",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/calls/-/raw/main/data/org.gnome.Calls.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++

