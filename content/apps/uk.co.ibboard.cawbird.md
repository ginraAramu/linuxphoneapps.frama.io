+++
title = "Cawbird"
description = "Twitter Client"
aliases = []
date = 2021-01-04
updated = 2023-04-22

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "IBBoard",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3",]
backends = []
services = [ "twitter",]
packaged_in = [ "alpine_3_17", "aur", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "News",]
programming_languages = [ "Vala", "C",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/IBBoard/cawbird"
homepage = "https://ibboard.co.uk/cawbird/"
bugtracker = "https://github.com/ibboard/cawbird/issues"
donations = ""
translations = "https://www.transifex.com/cawbird/cawbird/"
more_information = []
summary_source_url = "https://github.com/IBBoard/cawbird"
screenshots = [ "https://ibboard.co.uk/cawbird/appdata/screenshot1.jpg", "https://ibboard.co.uk/cawbird/appdata/screenshot2.jpg", "https://ibboard.co.uk/cawbird/appdata/screenshot3.jpg", "https://ibboard.co.uk/cawbird/appdata/screenshot4.jpg",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = 1
app_id = "uk.co.ibboard.cawbird.desktop"
scale_to_fit = "cawbird"
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/CodedOre/NewCaw/main/build-aux/Flatpak/uk.co.ibboard.Cawbird.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "cawbird",]
appstream_xml_url = "https://raw.githubusercontent.com/IBBoard/cawbird/master/data/uk.co.ibboard.cawbird.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++


### Notice

Was working perfectly since release 1.3.1, no longer working due to Space Karen's API changes; archived on 2023-01-23.  
A new Cawbird 2.0 with support for Mastodon is being worked on at [https://github.com/CodedOre/NewCaw](https://github.com/CodedOre/NewCaw).
