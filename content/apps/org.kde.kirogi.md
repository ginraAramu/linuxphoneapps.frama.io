+++
title = "Kirogi"
description = "Kirogi is a ground control application for drones. Take to the skies, open source-style."
aliases = []
date = 2019-10-22
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later", "LGPL-2.1-or-later",]
metadata_licenses = []
app_author = [ "KDE Kirogi Developers",]
categories = [ "drone control",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "aur", "gnuguix",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/kirogi"
homepage = "https://kirogi.org/"
bugtracker = "https://invent.kde.org/utilities/kirogi/-/issues/"
donations = ""
translations = ""
more_information = [ "https://blogs.kde.org/2019/09/08/introducing-kirogi-ground-control-application-drones", "https://mirror.kumi.systems/kde/files/akademy/2019/102-Taking_KDE_to_the_skies_Making_the_drone_ground_control_Kirogi.mp4", "https://invidio.us/channel/shoblubb", "https://mastodon.technology/tags/kirogi",]
summary_source_url = "https://invent.kde.org/utilities/kirogi"
screenshots = [ "https://kirogi.org/", "https://mastodon.technology/tags/kirogi",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kirogi"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kirogi",]
appstream_xml_url = "https://invent.kde.org/utilities/kirogi/-/raw/master/data/org.kde.kirogi.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++

