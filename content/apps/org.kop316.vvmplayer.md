+++
title = "Visual Voicemail"
description = "Visual Voicemail Player"
aliases = []
date = 2021-07-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Chris Talbot",]
categories = [ "visual voicemail",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "aur", "debian_12", "debian_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Audio", "Player",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/kop316/vvmplayer"
homepage = ""
bugtracker = "https://gitlab.com/kop316/vvmplayer/-/issues/"
donations = "https://liberapay.com/kop316/donate"
translations = ""
more_information = [ "https://gitlab.com/kop316/vvmplayer/-/wikis/home",]
summary_source_url = "https://gitlab.com/kop316/vvmplayer"
screenshots = [ "https://gitlab.com/kop316/vvmplayer/-/raw/main/data/metainfo/screenshot.png", "https://gitlab.com/kop316/vvmplayer/-/raw/main/data/metainfo/screenshot2.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kop316.vvmplayer"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "vvmplayer",]
appstream_xml_url = "https://gitlab.com/kop316/vvmplayer/-/raw/main/data/metainfo/org.kop316.vvmplayer.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Visual Voicemail Player is a GUI app that plays and deletes Visual voicemails retreived by Visual Voicemail Daemon (vvmd). [Source](https://gitlab.com/kop316/vvmplayer/-/raw/main/data/metainfo/org.kop316.vvmplayer.metainfo.xml.in)


### Notice

Requires vvmd (https://gitlab.com/kop316/vvmd) to be set up and your cellular provider to support Visual Voicemail.