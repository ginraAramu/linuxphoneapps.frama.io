+++
title = "Forklift"
description = "Video and audio download application"
aliases = []
date = 2020-08-26
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "johnn3y",]
categories = [ "video downloader",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/Johnn3y/Forklift"
homepage = ""
bugtracker = "https://github.com/Johnn3y/Forklift/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.github.Johnn3y.Forklift"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.Johnn3y.Forklift"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.Johnn3y.Forklift"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "forklift",]
appstream_xml_url = "https://raw.githubusercontent.com/Johnn3y/Forklift/master/data/com.github.Johnn3y.Forklift.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++

