+++
title = "Sublime Music"
description = "A native Subsonic client for Linux"
aliases = []
date = 2020-10-15
updated = 2023-04-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "sublime-music",]
categories = [ "audio streaming",]
mobile_compatibility = [ "3",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = [ "Subsonic", "ampache", "Revel", "Airsonic", "Gonic", "Navidrome",]
packaged_in = [ "alpine_edge", "aur", "debian_12", "debian_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "poetry",]

[extra]
repository = "https://github.com/sublime-music/sublime-music"
homepage = "https://sublimemusic.app"
bugtracker = "https://github.com/sublime-music/sublime-music/issues/"
donations = ""
translations = ""
more_information = [ "https://docs.sublimemusic.app/", "https://github.com/sublime-music/sublime-music/issues/97#issuecomment-1400643486",]
summary_source_url = "https://github.com/sublime-music/sublime-music"
screenshots = [ "https://docs.sublimemusic.app/_images/albums.png", "https://docs.sublimemusic.app/_images/artists.png", "https://docs.sublimemusic.app/_images/browse.png", "https://docs.sublimemusic.app/_images/playlists.png", "https://docs.sublimemusic.app/_images/play-queue.png", "https://docs.sublimemusic.app/_images/chromecasts.png", "https://docs.sublimemusic.app/_images/search.png", "https://docs.sublimemusic.app/_images/downloads.png", "https://docs.sublimemusic.app/_images/offline-mode.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "app.sublimemusic.SublimeMusic"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "sublime-music",]
appstream_xml_url = "https://raw.githubusercontent.com/sublime-music/sublime-music/master/sublime-music.metainfo.xml"
reported_by = "ula"
updated_by = "linmob"
+++



### Description

Desktop music player for *sonic server and compatible [Source](https://github.com/sublime-music/sublime-music)


### Notice

Scale-to-fit required and switch from portrait to landscape display to access certain menus. Works but interface not designed for a small screen. There is no .desktop, you have to run the command or create the .desktop. This [Issue comment](https://github.com/sublime-music/sublime-music/issues/97#issuecomment-1400643486) explains how to use a WIP libhandy branch.
