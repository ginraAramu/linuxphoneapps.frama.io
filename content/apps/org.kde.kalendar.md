+++
title = "Kalendar"
description = "A calendar application using Akonadi to sync with external services (NextCloud, GMail, ...) "
aliases = []
date = 2021-08-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community", "Clau Cambra and Carl Schwan",]
categories = [ "calendar",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = [ "Akonadi",]
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Office", "Calendar",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/pim/kalendar"
homepage = "https://apps.kde.org/kalendar/"
bugtracker = "https://invent.kde.org/pim/kalendar/-/issues/"
donations = ""
translations = ""
more_information = [ "https://claudiocambra.com/category/kde/", "https://claudiocambra.com/2022/02/12/kalendar-1-0-is-out/", "https://kde.org/announcements/gear/22.04.0/#new-arrivals",]
summary_source_url = "https://invent.kde.org/pim/kalendar"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.kde.kalendar/1.png", "https://img.linuxphoneapps.org/org.kde.kalendar/2.png", "https://img.linuxphoneapps.org/org.kde.kalendar/3.png", "https://img.linuxphoneapps.org/org.kde.kalendar/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kalendar"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kalendar",]
appstream_xml_url = "https://invent.kde.org/pim/kalendar/-/raw/master/org.kde.kalendar.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Notice

Great calendar app!
