+++
title = "Calculator"
description = "plasma mobile calculator application"
aliases = []
date = 2019-09-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Antoni Przybylik",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility", "Calculator",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/antoniprzybylik/calculator"
homepage = ""
bugtracker = "https://github.com/antoniprzybylik/calculator/issues/"
donations = ""
translations = ""
more_information = [ "https://invent.kde.org/antonip/calculator", "https://phabricator.kde.org/T8900",]
summary_source_url = "https://github.com/antoniprzybylik/calculator"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.calculator"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/antoniprzybylik/calculator/master/org.kde.calculator.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++



### Notice

Not in active development, may be replaced by Kalk. Inactive since November 2020 (GitHub)/July 2019 (invent).
