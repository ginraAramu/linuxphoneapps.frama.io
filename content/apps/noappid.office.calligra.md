+++
title = "Calligra Gemini"
description = "Office and graphic art suite by KDE"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "office",]
mobile_compatibility = [ "3",]
status = [ "unmaintained",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Office",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/office/calligra"
homepage = "https://apps.kde.org/calligragemini/"
bugtracker = "https://invent.kde.org/office/calligra/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/office/calligra"
screenshots = [ "https://cdn.kde.org/screenshots/calligra/words-touch-mode-with-notes-panel.png", "https://www.freeware-base.de/freewares-screenshots30600.html", "https://www.pro-linux.de/news/1/21650/calligra-gemini-vorgestellt.html",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "calligra",]
appstream_xml_url = "https://invent.kde.org/office/calligra/-/raw/master/gemini/org.kde.calligragemini.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Notice

Unmaintained according to https://apps.kde.org/calligragemini/
