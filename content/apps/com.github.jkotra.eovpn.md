+++
title = "eOVPN"
description = "OpenVPN Configuration Manager"
aliases = []
date = 2021-02-20
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Jagadeesh Kotra",]
categories = [ "system utilities",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = [ "OpenVPN",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/jkotra/eOVPN"
homepage = ""
bugtracker = "https://github.com/jkotra/eOVPN/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/jkotra/eOVPN"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.jkotra.eovpn"
scale_to_fit = "eovpn"
flathub = "https://flathub.org/apps/com.github.jkotra.eovpn"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "eovpn",]
appstream_xml_url = "https://raw.githubusercontent.com/jkotra/eOVPN/master/data/com.github.jkotra.eovpn.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Notice

scale-to-fit helps with properly scaling the settings menu to make it usable
