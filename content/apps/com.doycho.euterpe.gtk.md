+++
title = "Euterpe GTK"
description = "Mobile and desktop client for Euterpe."
aliases = []
date = 2022-03-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Doychin Atanasov",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "euterpe",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Player", "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/ironsmile/euterpe-gtk"
homepage = "https://listen-to-euterpe.eu/"
bugtracker = "https://github.com/ironsmile/euterpe-gtk/issues/"
donations = "https://github.com/sponsors/ironsmile"
translations = ""
more_information = [ "https://listen-to-euterpe.eu/docs",]
summary_source_url = "https://github.com/ironsmile/euterpe-gtk"
screenshots = [ "https://raw.githubusercontent.com/ironsmile/euterpe-gtk/master/repo/alpha-screenshots.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.doycho.euterpe.gtk"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.doycho.euterpe.gtk"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "euterpe",]
appstream_xml_url = "https://raw.githubusercontent.com/ironsmile/euterpe-gtk/master/data/com.doycho.euterpe.gtk.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

This is a convergent desktop and mobile client for Euterpe. It is developed with mobile Linux environments in mind. Such as Phosh and Plasma Mobile. But it is completely usable as a normal desktop application as well. [Source](https://github.com/ironsmile/euterpe-gtk)


### Notice

The text on the 'Login to your instance' page is a bit to wide, the rest is great. Playback of locally saved music is not yet supported.
