+++
title = "Mirage"
description = "A fancy, customizable, keyboard-operable Qt/QML & Python Matrix chat client for encrypted and decentralized communication."
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "mirukana",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "QtQuick",]
backends = [ "matrix-nio", "libolm",]
services = [ "Matrix",]
packaged_in = [ "aur", "debian_11", "devuan_4_0", "pureos_landing",]
freedesktop_categories = [ "Qt", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "QML", "Python",]
build_systems = [ "qmake",]

[extra]
repository = "https://github.com/mirukana/mirage"
homepage = ""
bugtracker = "https://github.com/mirukana/mirage/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/mirukana/mirage"
screenshots = [ "https://github.com/mirukana/mirage/raw/master/docs/screenshots/05-main-pane-small.png", "https://github.com/mirukana/mirage/raw/master/docs/screenshots/06-chat-small.png", "https://github.com/mirukana/mirage/raw/master/docs/screenshots/07-room-pane-small.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.mirukana.mirage"
scale_to_fit = ""
flathub = ""
flatpak_link = "https://github.com/mirukana/mirage/tree/master/packaging/flatpak"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "matrix-mirage",]
appstream_xml_url = "https://raw.githubusercontent.com/mirukana/mirage/master/packaging/mirage.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

A fancy, customizable, keyboard-operable Matrix chat client for encrypted and decentralized communication. Written in Qt/QML and Python, currently in alpha. [Source](https://github.com/mirukana/mirage)


### Notice

Fits the screen, but sometimes navigating the UI requires non-obvious gestures. Check out Moment if you’re looking for a maintained fork of Mirage.
