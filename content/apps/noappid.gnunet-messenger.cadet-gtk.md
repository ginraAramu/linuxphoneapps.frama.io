+++
title = "cadet-gtk"
description = "A GTK based GUI for the CADET subsystem of GNUnet."
aliases = []
date = 2020-10-11
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "gnunet-messenger",]
categories = [ "chat",]
mobile_compatibility = [ "needs testing",]
status = [ "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "GNUnet CADET",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "P2P",]
programming_languages = [ "C",]
build_systems = [ "custom",]

[extra]
repository = "https://gitlab.com/gnunet-messenger/cadet-gtk"
homepage = ""
bugtracker = "https://gitlab.com/gnunet-messenger/cadet-gtk/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/gnunet-messenger/cadet-gtk"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "cadet-gtk",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++
