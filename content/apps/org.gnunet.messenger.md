+++
title = "GNUnet Messenger"
description = "A GTK based GUI for the Messenger service of GNUnet."
aliases = []
date = 2021-12-24
updated = 2023-04-22

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNUnet e.V.",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "libgnunetchat",]
services = [ "GNUnet Messenger",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://git.gnunet.org/messenger-gtk.git"
homepage = "https://www.gnunet.org"
bugtracker = "https://bugs.gnunet.org/my_view_page.php"
donations = "https://www.gnunet.org/en/ev.html"
translations = ""
more_information = [ "https://git.gnunet.org/messenger-gtk.git/tree/README.md", "https://www.reddit.com/r/pinephone/comments/rk5zyb/gnunet_messenger_for_mobile_linux/", "https://thejackimonster.blogspot.com/2022/02/gnunet-messenger-api-february.html", "https://thejackimonster.blogspot.com/2022/03/gnunet-messenger-api-march.html", "https://thejackimonster.blogspot.com/2022/09/gnunet-messenger-api-september.html", "https://thejackimonster.blogspot.com/2022/12/development-in-2022.html",]
summary_source_url = "https://gitlab.com/gnunet-messenger/messenger-gtk"
screenshots = [ "https://gitlab.com/gnunet-messenger/messenger-gtk/-/raw/master/screenshots/about-page.png", "https://gitlab.com/gnunet-messenger/messenger-gtk/-/raw/master/screenshots/account-selection.png", "https://gitlab.com/gnunet-messenger/messenger-gtk/-/raw/master/screenshots/contact-info.png", "https://gitlab.com/gnunet-messenger/messenger-gtk/-/raw/master/screenshots/delete-messages.png", "https://gitlab.com/gnunet-messenger/messenger-gtk/-/raw/master/screenshots/voice-recording.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnunet.Messenger"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnunet.Messenger"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = "https://git.gnunet.org/messenger-gtk.git/plain/snap/snapcraft.yaml"
repology = [ "messenger-gtk",]
appstream_xml_url = "https://git.gnunet.org/messenger-gtk.git/plain/resources/org.gnunet.Messenger.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"
+++


### Description

Messenger-GTK is a convergent GTK messaging application using the GNUnet Messenger service. The goal is to provide private and secure communication between any group of devices.

The application provides the following features:

*   Creating direct chats and group chats
*   Managing your contacts and groups
*   Invite contacts to a group
*   Sending text messages
*   Sending voice recordings
*   Sharing files privately
*   Deleting messages with any custom delay
*   Renaming contacts
*   Exchanging contact details physically
*   Verifying contact identities
*   Switching between different accounts

Chats will generally created as opt-in. So you can decide who may contact you directly and who does not, accepting to a direct chat. Leaving a chat is also always possible. [Source](https://git.gnunet.org/messenger-gtk.git/plain/resources/org.gnunet.Messenger.appdata.xml)

