+++
title = "Kaidan"
description = "Kaidan - A user-friendly XMPP client for every device!"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Kaidan Developers & Contributors",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = [ "XMPP",]
packaged_in = [ "alpine_3_17", "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "flathub", "gnuguix", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/network/kaidan"
homepage = "https://www.kaidan.im/"
bugtracker = "https://invent.kde.org/network/kaidan/-/issues/"
donations = ""
translations = ""
more_information = [ "https://www.kaidan.im/2021/05/28/kaidan-0.8.0/",]
summary_source_url = "https://invent.kde.org/network/kaidan"
screenshots = [ "https://www.kaidan.im/images/screenshot.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "im.kaidan.kaidan"
scale_to_fit = ""
flathub = "https://flathub.org/apps/im.kaidan.kaidan"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kaidan",]
appstream_xml_url = "https://invent.kde.org/network/kaidan/-/raw/master/misc/im.kaidan.kaidan.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++



### Description

Kaidan is a simple, user-friendly and modern chat client. It uses the open communication protocol XMPP (Jabber). The user interface makes use of Kirigami and QtQuick, while the back-end of Kaidan is entirely written in C++ using Qt and the Qt-based XMPP library QXmpp. Kaidan runs on mobile and desktop systems including Linux, Windows, macOS, Android, Plasma Mobile and Ubuntu Touch. Unfortunately, we are not able to provide builds for all platforms at the moment due to little developer resources. Kaidan does not have all basic features yet and has still some stability issues. Do not expect it to be as good as the currently dominating instant messaging clients. If you are interested in the technical features, you can have a look at Kaidan's [overview page](https://xmpp.org/software/clients/kaidan/) including XEPs and RFCs. [Source](https://invent.kde.org/network/kaidan)
