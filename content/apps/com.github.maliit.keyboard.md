+++
title = "Maliit Keyboard"
description = "Maliit Keyboard, a free software virtual keyboard for Linux"
aliases = []
date = 2020-10-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Maliit Team",]
categories = [ "virtual keyboard",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/maliit/keyboard"
homepage = "https://maliit.github.io"
bugtracker = "https://github.com/maliit/keyboard/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/maliit/keyboard"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.maliit.keyboard"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "maliit-keyboard",]
appstream_xml_url = "https://raw.githubusercontent.com/maliit/keyboard/master/com.github.maliit.keyboard.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Maliit Keyboard is a free software virtual keyboard for Linux systems with Wayland and X11 display servers. It supports many different languages and emoji. It is implemented as a plug-in for Maliit Framework.  Maliit Keyboard evolved from the [reference keyboard plug-in](https://github.com/maliit/plugins), [Ubuntu Keyboard](https://launchpad.net/ubuntu-keyboard) and [Lomiri Keyboard](https://github.com/maliit/keyboard/pull/60). Ubuntu Keyboard was a fork of the reference plugin which was taking into account the special UI/UX needs of Ubuntu Phone. [Source](https://github.com/maliit/keyboard)
