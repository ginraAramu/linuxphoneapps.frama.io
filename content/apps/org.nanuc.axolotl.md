+++
title = "Axolotl"
description = "Axolotl is a crossplattform Signal client"
aliases = []
date = 2020-10-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "nanu-c",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Electron",]
backends = []
services = [ "Signal",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Go", "Vue",]
build_systems = [ "various", "custom",]

[extra]
repository = "https://github.com/nanu-c/axolotl"
homepage = "https://axolotl.chat/"
bugtracker = "https://github.com/nanu-c/axolotl/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.mobian-project.org/doku.php?id=axolotl",]
summary_source_url = "https://github.com/nanu-c/axolotl"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.nanuc.Axolotl"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.nanuc.Axolotl"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "axolotl",]
appstream_xml_url = "https://raw.githubusercontent.com/nanu-c/axolotl/main/flatpak/org.nanuc.Axolotl.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++



### Description

A Signal compatible cross plattform client written in Go and Vuejs [Source](https://github.com/nanu-c/axolotl)


### Notice

To install it on Mobian see https://github.com/nuehm-arno/axolotl-mobian-package
