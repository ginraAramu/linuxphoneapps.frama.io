+++
title = "Pikasso"
description = "Simple drawing programs using Kirigami for the UI and Rust for the rendering."
aliases = []
date = 2021-02-03
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = []
app_author = [ "KDE",]
categories = [ "drawing",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "KDE", "Graphics", "RasterGraphics",]
programming_languages = [ "Cpp", "Rust", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/graphics/pikasso"
homepage = ""
bugtracker = "https://invent.kde.org/graphics/pikasso/-/issues/"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2021/01/25/pikasso-a-simple-drawing-application-in-qtquick-with-rust/",]
summary_source_url = "https://invent.kde.org/graphics/pikasso"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.pikasso"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "pikasso",]
appstream_xml_url = "https://invent.kde.org/graphics/pikasso/-/raw/master/org.kde.pikasso.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++

