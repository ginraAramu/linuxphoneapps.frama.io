+++
title = "PicPlanner"
description = "Calculate the position of the Sun, Moon and Milky Way to plan the position and time for a photograph."
aliases = []
date = 2022-09-22
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Zwarf",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Photography",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/Zwarf/picplanner"
homepage = ""
bugtracker = "https://gitlab.com/Zwarf/picplanner/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = [ "https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-1.png", "https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-2.png", "https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-3.png", "https://gitlab.com/Zwarf/picplanner/-/raw/main/screenshots/picplanner-4.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/de.zwarf.picplanner/1.png", "https://img.linuxphoneapps.org/de.zwarf.picplanner/2.png", "https://img.linuxphoneapps.org/de.zwarf.picplanner/3.png", "https://img.linuxphoneapps.org/de.zwarf.picplanner/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "de.zwarf.picplanner"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.zwarf.picplanner"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "picplanner",]
appstream_xml_url = "https://gitlab.com/Zwarf/picplanner/-/raw/main/data/de.zwarf.picplanner.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

A GTK application for photographers using GNU Linux or especially Linux phones. It can be used to calculate the position of the Sun, Moon and Milky Way in order to plan the position and time for a photograph. [Source](https://gitlab.com/Zwarf/picplanner)
