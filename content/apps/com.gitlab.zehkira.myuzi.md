+++
title = "Myuzi"
description = "Stream music from YouTube with no ads."
aliases = []
date = 2022-09-03
updated = 2023-04-23

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "zehkira",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "gone",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "yt-dlp", "GStreamer",]
services = [ "YouTube",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Audio", "Network", "Player",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "http://web.archive.org/web/20221222151535/https://gitlab.com/zehkira/myuzi"
homepage = ""
bugtracker = "http://web.archive.org/web/20221127212430/https://gitlab.com/zehkira/myuzi/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.gitlab.zehkira.Myuzi"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "myuzi",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

Myuzi is a free and open source music streaming app for Linux. Unlike Spotify, Myuzi: - does not require an account, - has no advertisements, - is completely free, - uses your system GTK theme. All the music is streamed directly from YouTube [Source](http://web.archive.org/web/20221222151535/https://gitlab.com/zehkira/myuzi)


### Notice

Despite this apps self-description, it is not a [Spotify client](https://linuxphoneapps.org/services/spotify/). It has been archived and been succeded by [Monophony](com.gitlab.zehkira.monophony), as of 2023-04-23 the source has been deleted and appears to be gone.
