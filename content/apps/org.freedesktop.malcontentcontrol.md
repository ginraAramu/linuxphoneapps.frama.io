+++
title = "Malcontent-control"
description = "malcontent implements support for restricting the type of content accessible to non-administrator accounts on a Linux system."
aliases = []
date = 2020-11-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "The GNOME Project",]
categories = [ "parental controls",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.freedesktop.org/pwithnall/malcontent"
homepage = ""
bugtracker = "https://gitlab.freedesktop.org/pwithnall/malcontent/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.freedesktop.org/pwithnall/malcontent"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.freedesktop.MalcontentControl"
scale_to_fit = "Malcontent-control"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "malcontent", "malcontent-ui",]
appstream_xml_url = "https://gitlab.freedesktop.org/pwithnall/malcontent/-/raw/main/malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Notice

GUI looks fine, but neither Phosh nor Plasma Mobile are multi-user ready yet, which makes this technically useless for now.