+++
title = "Breathing"
description = "Relax, focus, and become stress-free."
aliases = []
date = 2021-08-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Dave Patrick",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/SeaDve/Breathing"
homepage = ""
bugtracker = "https://github.com/SeaDve/Breathing/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/SeaDve/Breathing"
screenshots = [ "https://github.com/SeaDve/Breathing/tree/main/screenshots",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.seadve.Breathing"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.seadve.Breathing"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "breathing",]
appstream_xml_url = "https://raw.githubusercontent.com/SeaDve/Breathing/main/data/io.github.seadve.Breathing.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Breathing is a very simple application that guides your breathing pattern. This pattern is recommended by experts that will help ease your anxiety. It also provides a calming sound to make it much easier to relax. [Source](https://github.com/SeaDve/Breathing)


### Notice

Scales perfectly since 0.1.1.
