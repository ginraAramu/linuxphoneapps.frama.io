+++
title = "Satellite"
description = "Check your GPS reception and save your tracks"
aliases = [ "apps/org.codeberg.tpikonen.satellite/",]
date = 2021-08-02
updated = 2023-04-22

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "tpikonen",]
categories = [ "geography",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "ModemManager", "gnss-share",]
services = []
packaged_in = [ "aur", "debian_12", "debian_unstable", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Geoscience", "Monitor",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://codeberg.org/tpikonen/satellite"
homepage = "https://codeberg.org/tpikonen/satellite"
bugtracker = "https://codeberg.org/tpikonen/satellite/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/tpikonen/satellite"
screenshots = [ "https://raw.githubusercontent.com/flathub/page.codeberg.tpikonen.satellite/master/screenshot-fix.png", "https://raw.githubusercontent.com/flathub/page.codeberg.tpikonen.satellite/master/screenshot-log.png", "https://raw.githubusercontent.com/flathub/page.codeberg.tpikonen.satellite/master/screenshot-snr.png", "https://raw.githubusercontent.com/flathub/page.codeberg.tpikonen.satellite/master/screenshot-track.png",]
screenshots_img = []
all_features_touch = 1
intended_for_mobile = ""
app_id = "page.codeberg.tpikonen.satellite"
scale_to_fit = ""
flathub = "https://flathub.org/apps/page.codeberg.tpikonen.satellite"
flatpak_link = ""
flatpak_recipe = "https://codeberg.org/tpikonen/satellite/raw/branch/main/flatpak/page.codeberg.tpikonen.satellite.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "satellite-gtk",]
appstream_xml_url = "https://codeberg.org/tpikonen/satellite/raw/branch/main/data/appdata.xml"
reported_by = "tpikonen"
updated_by = "linmob"
+++


### Description

Satellite is an adaptive GTK / libhandy application which displays global navigation satellite system
(GNSS: GPS et al.) data obtained from [ModemManager](https://www.freedesktop.org/wiki/Software/ModemManager/)
or [gnss-share](https://gitlab.com/postmarketOS/gnss-share). It can also save your position to a GPX-file. [Source](https://codeberg.org/tpikonen/satellite)
