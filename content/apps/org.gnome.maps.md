+++
title = "GNOME Maps"
description = "Maps is a map application for GNOME."
aliases = []
date = 2021-05-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "maps and navigation", "geography",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "libshumate",]
services = [ "openstreetmap",]
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Maps",]
programming_languages = [ "JavaScript", "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-maps"
homepage = "https://wiki.gnome.org/Apps/Maps"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-maps/-/issues/"
donations = "https://www.gnome.org/donate/"
translations = ""
more_information = [ "https://linmob.net/gnome-3-38-and-linux-smartphones/#gnome-maps", "https://apps.gnome.org/app/org.gnome.Maps/", "https://ml4711.blogspot.com/2021/05/spring-maps.html",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-maps"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.maps/1.png", "https://img.linuxphoneapps.org/org.gnome.maps/2.png", "https://img.linuxphoneapps.org/org.gnome.maps/3.png", "https://img.linuxphoneapps.org/org.gnome.maps/4.png", "https://img.linuxphoneapps.org/org.gnome.maps/5.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Maps"
scale_to_fit = "org.gnome.Maps"
flathub = "https://flathub.org/apps/org.gnome.Maps"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-maps",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-maps/-/raw/main/data/org.gnome.Maps.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Maps gives you quick access to maps all across the world. It allows you to quickly find the place you're looking for by searching for a city or street, or locate a place to meet a friend. Maps uses the collaborative OpenStreetMap database, made by hundreds of thousands of people across the globe. [Source](https://gitlab.gnome.org/GNOME/gnome-maps/-/raw/main/data/org.gnome.Maps.appdata.xml.in.in)


### Notice

Scales fine, except for the navigation view - there's a slight improvement now compared to the GTK3 past.
