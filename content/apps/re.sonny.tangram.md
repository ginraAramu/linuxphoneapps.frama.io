+++
title = "Tangram"
description = "Browser for your pinned tabs"
aliases = []
date = 2021-06-06
updated = 2023-02-11

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sonny Piers",]
categories = [ "web browser",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "gentoo", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "WebBrowser",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/sonnyp/Tangram"
homepage = "https://tangram.sonny.re"
bugtracker = "https://github.com/sonnyp/Tangram/issues/"
donations = "https://ko-fi.com/sonnyp"
translations = "https://hosted.weblate.org/engage/tangram/"
more_information = [ "https://wiki.mobian-project.org/doku.php?id=tangram", "https://apps.gnome.org/app/re.sonny.Tangram/",]
summary_source_url = "https://raw.githubusercontent.com/sonnyp/Tangram/main/data/appdata/re.sonny.Tangram.metainfo.xml"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/re.sonny.tangram/1.png", "https://img.linuxphoneapps.org/re.sonny.tangram/2.png", "https://img.linuxphoneapps.org/re.sonny.tangram/3.png", "https://img.linuxphoneapps.org/re.sonny.tangram/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "re.sonny.Tangram"
scale_to_fit = "re.sonny.Tangram"
flathub = "https://flathub.org/apps/re.sonny.Tangram"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "tangram",]
appstream_xml_url = "https://raw.githubusercontent.com/sonnyp/Tangram/main/data/appdata/re.sonny.Tangram.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

Tangram is a new kind of browser. It is designed to organize and run your Web applications. Each tab is persistent and independent. You can set multiple tabs with different accounts for the same application. [Source](https://github.com/sonnyp/Tangram)


### Notice

Just a few pixels too wide, see [issue](https://github.com/sonnyp/Tangram/issues/233). Used to be GTK3 before release 2.0.
