+++
title = "Notejot"
description = "Stupidly-simple notes app."
aliases = []
date = 2021-02-20
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lains",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_rawhide", "flathub", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/lainsce/notejot/"
homepage = ""
bugtracker = "https://github.com/lainsce/notejot/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/lainsce/notejot/"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.lainsce.notejot/1.png", "https://img.linuxphoneapps.org/io.github.lainsce.notejot/2.png", "https://img.linuxphoneapps.org/io.github.lainsce.notejot/3.png", "https://img.linuxphoneapps.org/io.github.lainsce.notejot/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.lainsce.Notejot"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.lainsce.Notejot"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "notejot",]
appstream_xml_url = "https://raw.githubusercontent.com/lainsce/notejot/main/data/io.github.lainsce.Notejot.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

A stupidly-simple notes application for any type of short term notes or ideas. ? Color your notes in 8 different colors, ? Classify them in notebooks, ? Format text to your liking, ? Pin your most important ones. [Source](https://raw.githubusercontent.com/lainsce/notejot/main/data/io.github.lainsce.Notejot.metainfo.xml.in)
