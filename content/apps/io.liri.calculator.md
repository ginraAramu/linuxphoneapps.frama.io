+++
title = "Liri Calculator"
description = "Liri Calculator is a cross-platform, Material Design calculator, currently in  alpha  stage. "
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "The Liri developers",]
categories = [ "calculator",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "Utility", "Calculator",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/lirios/calculator"
homepage = "https://liri.io/apps/calculator/"
bugtracker = "https://github.com/lirios/calculator/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://liri.io/apps/calculator/"
screenshots = [ "https://liri.io/apps/calculator/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.liri.Calculator"
scale_to_fit = "io.liri.Calculator"
flathub = "https://flathub.org/apps/io.liri.Calculator"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "liri-calculator",]
appstream_xml_url = "https://raw.githubusercontent.com/lirios/calculator/develop/data/io.liri.Calculator.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++

