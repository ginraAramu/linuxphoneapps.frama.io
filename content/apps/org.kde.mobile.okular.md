+++
title = "Okular"
description = "Okular is a universal document viewer developed by KDE."
aliases = []
date = 2019-02-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "document viewer", "pdf viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Office", "Viewer",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/graphics/okular"
homepage = "https://userbase.kde.org/Okular"
bugtracker = "https://invent.kde.org/graphics/okular/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://userbase.kde.org/Okular"
screenshots = [ "https://userbase.kde.org/Okular",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.mobile.okular"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "okular",]
appstream_xml_url = "https://invent.kde.org/graphics/okular/-/raw/master/mobile/app/org.kde.okular.kirigami.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++

