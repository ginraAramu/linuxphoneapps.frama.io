+++
title = "Converter"
description = "Convert and manipulate images"
aliases = []
date = 2023-04-16

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Khaleel Al-Adhami",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "ImageMagick",]
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "ImageProcessing",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/adhami3310/Converter"
homepage = "https://gitlab.com/adhami3310/Converter"
bugtracker = "https://gitlab.com/adhami3310/Converter/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/adhami3310/Converter/-/raw/main/data/io.gitlab.adhami3310.Converter.metainfo.xml.in"
screenshots = [ "https://gitlab.com/adhami3310/Converter/-/raw/main/data/screenshots/0.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/1.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/2.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/3.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/4.png",]
all_features_touch = true
intended_for_mobile = ""
app_id = "io.gitlab.adhami3310.Converter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.adhami3310.Converter"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "converter",]
appstream_xml_url = "https://gitlab.com/adhami3310/Converter/-/raw/main/data/io.gitlab.adhami3310.Converter.metainfo.xml.in"
reported_by = "linmob"
updated_by = ""
+++


### Description



### Notice
