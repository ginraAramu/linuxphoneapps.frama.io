+++
title = "Liri App Center"
description = "App Center for Liri OS"
aliases = []
date = 2020-11-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "lirios",]
categories = [ "app store",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/lirios/appcenter"
homepage = ""
bugtracker = "https://github.com/lirios/appcenter/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/lirios/appcenter"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.liri.AppCenter"
scale_to_fit = "io.liri.AppCenter"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "liri-appcenter",]
appstream_xml_url = "https://raw.githubusercontent.com/lirios/appcenter/develop/src/app/io.liri.AppCenter.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Notice

Fine with scale-to-fit, best reason to use it is that it provides a proper alphabetical overview of flathubs software.
