+++
title = "Smile"
description = "An emoji picker"
aliases = []
date = 2022-05-31
updated = 2023-02-24

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lorenzo Paderi",]
categories = [ "emoji picker",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/mijorus/smile"
homepage = ""
bugtracker = "https://github.com/mijorus/smile/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/mijorus/smile/blob/master/data/it.mijorus.smile.appdata.xml.in"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/it.mijorus.smile/1.png", "https://img.linuxphoneapps.org/it.mijorus.smile/2.png", "https://img.linuxphoneapps.org/it.mijorus.smile/3.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "it.mijorus.smile"
scale_to_fit = ""
flathub = "https://flathub.org/apps/it.mijorus.smile"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "smile",]
appstream_xml_url = "https://raw.githubusercontent.com/mijorus/smile/master/data/it.mijorus.smile.appdata.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

Smile is a simple emoji picker for linux with custom tags support. [Source](https://github.com/mijorus/smile)


### Notice

Was GTK 3 pre 2.0.
