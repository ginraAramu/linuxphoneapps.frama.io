+++
title = "MooTube"
description = "A YouTube App for Mobile Linux."
aliases = []
date = 2021-06-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "ninebysix",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = [ "mpv",]
services = [ "YouTube",]
packaged_in = []
freedesktop_categories = [ "Network", "Video", "Player",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://github.com/ninebysix/MooTube"
homepage = ""
bugtracker = "https://github.com/ninebysix/MooTube/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/ninebysix/MooTube"
screenshots = [ "https://github.com/ninebysix/MooTube",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++


### Description

Browse and play media from YouTube without the need to sign-in. With the recent popularity of Linux Phones and the desire to stream media from YouTube, LinMoTube was born! Supports both YouTube Video mode and YouTube Music mode with a convenient toggle switch! [Source](https://github.com/ninebysix/MooTube)


### Notice

Previously called LinMoTube