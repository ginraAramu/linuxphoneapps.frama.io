+++
title = "Apps"
description = "A list of apps that work on Linux Phones like the PinePhone or Librem 5."
date = "2022-04-14"
sort_by = "title"
template = "apps/section.html"
page_template = "apps/page.html"
generate_feed = true
in_search_index = true

+++

Not finding what your looking for? Searching [other lists](@/docs/resources/other-lists-and-web-apps.md) or [the list of apps that still need to be evaluated and added](@/lists/apps-to-be-added.md) may help! 
