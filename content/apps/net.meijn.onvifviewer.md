+++
title = "ONVIFViewer"
description = "View and control network cameras using the ONVIF protocol"
aliases = []
date = 2019-02-02
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Casper Meijn",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = [ "unmaintained",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Qt", "KDE", "Photography",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://gitlab.com/caspermeijn/onvifviewer"
homepage = ""
bugtracker = "https://gitlab.com/caspermeijn/onvifviewer/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/net.meijn.onvifviewer"
screenshots = [ "https://flathub.org/apps/net.meijn.onvifviewer",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "net.meijn.onvifviewer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.meijn.onvifviewer"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "onvifviewer",]
appstream_xml_url = "https://gitlab.com/caspermeijn/onvifviewer/-/raw/master/desktop/net.meijn.onvifviewer.appdata.xml.in"
reported_by = "caspermeijn"
updated_by = "script"
+++

