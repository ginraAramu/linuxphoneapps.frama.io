+++
title = "Marker"
description = "A gtk3 markdown editor"
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "fabiocolacio",]
categories = [ "writing",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "fedora_38", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "JavaScript", "C",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/fabiocolacio/Marker"
homepage = ""
bugtracker = "https://github.com/fabiocolacio/Marker/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/fabiocolacio/Marker"
screenshots = [ "https://wiki.mobian-project.org/doku.php?id=marker",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.fabiocolacio.marker"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.fabiocolacio.marker"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "marker",]
appstream_xml_url = "https://raw.githubusercontent.com/fabiocolacio/Marker/master/data/com.github.fabiocolacio.marker.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Powerful markdown editor for the GNOME desktop. [Source](https://flathub.org/apps/com.github.fabiocolacio.marker)
