+++
title = "PineBattery"
description = "GTK app for monitoring the PinePhone and PineTab battery."
aliases = []
date = 2021-04-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "jasong-fr",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utiltiy",]
programming_languages = [ "Python",]
build_systems = [ "custom",]

[extra]
repository = "https://github.com/JasonG-FR/PineBattery"
homepage = ""
bugtracker = "https://github.com/JasonG-FR/PineBattery/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/JasonG-FR/PineBattery"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Cloning the repo and running install.sh is enough to try this simple battery monitor.