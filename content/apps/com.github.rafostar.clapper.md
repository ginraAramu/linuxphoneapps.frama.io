+++
title = "Clapper"
description = "A GNOME media player build using GJS with GTK4 toolkit. The media player is using GStreamer as a media backend and renders everything via OpenGL."
aliases = []
date = 2021-05-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Rafa? Dzi?giel",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "flathub", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Video", "Player",]
programming_languages = [ "Javascript", "C",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/Rafostar/clapper"
homepage = ""
bugtracker = "https://github.com/Rafostar/clapper/issues/"
donations = ""
translations = ""
more_information = [ "https://briandaniels.me/2021/07/06/hardware-accelerated-video-playback-on-the-pinephone-with-clapper.html",]
summary_source_url = "https://github.com/Rafostar/clapper"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.rafostar.Clapper"
scale_to_fit = "com.github.rafostar.Clapper"
flathub = "https://flathub.org/apps/com.github.rafostar.Clapper"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "clapper",]
appstream_xml_url = "https://raw.githubusercontent.com/Rafostar/clapper/master/data/com.github.rafostar.Clapper.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Notice

Supports hardware accelerated playback.
