+++
title = "Font Downloader"
description = "A GTK app that searches for Google Fonts and installs them."
aliases = []
date = 2020-10-26
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Gustavo Peredo",]
categories = [ "font downloader",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Google Fonts",]
packaged_in = [ "flathub", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "TextTools", "Dictionary",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/GustavoPeredo/font-downloader"
homepage = ""
bugtracker = "https://github.com/GustavoPeredo/font-downloader/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gustavoperedo.FontDownloader/",]
summary_source_url = "https://github.com/GustavoPeredo/font-downloader"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gustavoperedo.FontDownloader"
scale_to_fit = "fontdownloader"
flathub = "https://flathub.org/apps/org.gustavoperedo.FontDownloader"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "font-downloader",]
appstream_xml_url = "https://raw.githubusercontent.com/GustavoPeredo/Font-Downloader/master/data/org.gustavoperedo.FontDownloader.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++

