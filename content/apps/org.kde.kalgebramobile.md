+++
title = "KAlgebra"
description = "Graph Calculator"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "education",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Science", "Math",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/education/kalgebra"
homepage = "https://edu.kde.org/kalgebra/"
bugtracker = "https://invent.kde.org/education/kalgebra/-/issues/"
donations = "https://kde.org/de/community/donations/?app=kalgebra&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/education/kalgebra/-/raw/master/mobile/org.kde.kalgebramobile.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kalgebra/kalgebra-mobile.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kalgebramobile"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kalgebra"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kalgebra",]
appstream_xml_url = "https://invent.kde.org/education/kalgebra/-/raw/master/mobile/org.kde.kalgebramobile.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++



### Description

KAlgebra is an application that can replace your graphing calculator. It has numerical, logical, symbolic, and analysis features that let you calculate mathematical expressions on the console and graphically plot the results in 2D or 3D. [Source](https://invent.kde.org/education/kalgebra/-/raw/master/mobile/org.kde.kalgebramobile.appdata.xml)
