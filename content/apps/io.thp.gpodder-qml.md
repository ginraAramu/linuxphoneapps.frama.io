+++
title = "gPodder QML UI"
description = "gPodder 4 QML UI Reference Implementation"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "not specified",]
metadata_licenses = []
app_author = [ "gpodder",]
categories = [ "podcast client",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Qt", "Network", "Audio", "Feeds",]
programming_languages = [ "QML", "Python",]
build_systems = [ "make",]

[extra]
repository = "https://github.com/gpodder/gpodder-ui-qml"
homepage = "https://invent.kde.org/jbbgameich/gpodder-qml"
bugtracker = "https://github.com/gpodder/gpodder-ui-qml/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/gpodder/gpodder-ui-qml"
screenshots = [ "https://phabricator.kde.org/T9139",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.thp.gpodder-qml"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gpodder", "gpodder-adaptive",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"
+++




### Notice

Did not work properly, but the UI scales nicely