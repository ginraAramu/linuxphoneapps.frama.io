+++
title = "Mepo"
description = "Fast, simple, and hackable OSM map viewer for Linux. Designed with the Pinephone & mobile linux in mind; works both offline and online."
aliases = []
date = 2022-03-25
updated = 2022-10-02

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Miles Alan",]
categories = [ "maps and navigation", "geography",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "SDL",]
backends = []
services = [ "openstreetmap",]
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Utility", "Maps",]
programming_languages = [ "Zig",]
build_systems = [ "zig",]

[extra]
repository = "https://git.sr.ht/~mil/mepo"
homepage = "https://mepo.milesalan.com/"
bugtracker = "https://todo.sr.ht/~mil/mepo-tickets"
donations = ""
translations = ""
more_information = [ "https://mepo.milesalan.com/guides.html", "https://mepo.milesalan.com/userguide.html", "https://mepo.milesalan.com/demos.html", "https://mepo.milesalan.com/demos.html",]
summary_source_url = "https://sr.ht/~mil/mepo/"
screenshots = [ "https://media.lrdu.org/mepo_demos/mepo_demo_0.3_pois.png", "https://media.lrdu.org/mepo_demos/mepo_demo_0.3_routing.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.milesalan.mepo"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.milesalan.mepo"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "mepo",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/com.milesalan.mepo/master/com.milesalan.mepo.metainfo.xml"
reported_by = "milesalan"
updated_by = "linmob"
+++



### Description

Mepo is a fast, simple, and hackable OSM map viewer for desktop & mobile Linux devices (like the PinePhone, Librem 5, pmOS devices etc.) and both environment's various user interfaces (Wayland & X inclusive); works on Phosh and Sxmo currently. Mepo works both offline and online, features a minimalist both touch/mouse and keyboard compatible interface, and offers a UNIX-philosophy inspired underlying design, exposing a powerful command language called Mepolang capable of being scripted to provide things like custom bounding-box search scripts, bookmarks, and more. [Source](https://sr.ht/~mil/mepo/)


### Notice

Phosh & Sxmo compatible
