+++
title = "Celluloid"
description = "A simple GTK+ frontend for mpv"
aliases = []
date = 2020-11-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "The Celluloid Developers",]
categories = [ "video player",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = [ "mpv",]
services = []
packaged_in = [ "alpine_3_17", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "AudioVideo", "Video", "Player",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/celluloid-player/celluloid"
homepage = "https://celluloid-player.github.io/"
bugtracker = "https://github.com/celluloid-player/celluloid/issues/"
donations = ""
translations = "https://hosted.weblate.org/projects/celluloid/"
more_information = [ "https://wiki.mobian-project.org/doku.php?id=celluloid",]
summary_source_url = "https://github.com/celluloid-player/celluloid"
screenshots = [ "https://celluloid-player.github.io/images/screenshot-0.png", "https://celluloid-player.github.io/images/screenshot-1.png", "https://celluloid-player.github.io/images/screenshot-2.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.celluloid_player.Celluloid"
scale_to_fit = "io.github.celluloid_player.Celluloid"
flathub = "https://flathub.org/apps/io.github.celluloid_player.Celluloid"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "celluloid",]
appstream_xml_url = "https://raw.githubusercontent.com/celluloid-player/celluloid/master/data/io.github.celluloid_player.Celluloid.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Notice

Scales well, except for settings, which are usable, but slightly too wide. Playback did not work with the 0.24 flathub flatpak on postmarketOS 22.06.1.
