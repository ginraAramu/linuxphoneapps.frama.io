+++
title = "Notorious"
description = "Keyboard centric notes"
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Gabriele Musco",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GabMus/notorious"
homepage = "https://notorious.gabmus.org/"
bugtracker = "https://gitlab.gnome.org/GabMus/notorious/-/issues/"
donations = "https://liberapay.com/gabmus/donate"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GabMus/notorious"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gabmus.notorious"
scale_to_fit = "notorious"
flathub = "https://flathub.org/apps/org.gabmus.notorious"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "notorious",]
appstream_xml_url = "https://gitlab.gnome.org/GabMus/notorious/-/raw/master/data/org.gabmus.notorious.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

Notorious can be controlled entirely with your keyboard. No need to move your hand to the mouse, just open it up and dump your brain. Many like to use Markdown to write their notes. Hack I even use it on paper nowadays. In Notorious you can write your notes however you want, but if you like Markdown, you can easily enable syntax highlighting. Darkmode, Autosave are other great features. [Source](https://gitlab.gnome.org/GabMus/notorious)


### Notice

Everything is adjusted for mobile phones, only the 'shortcuts' menu is not (which is of limited use anyway)
