+++
title = "Date of Catholic Easter"
description = "Program for calculating the date of Catholic Easter"
aliases = []
date = 2023-02-24

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alex Kryuchkov",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/alexkdeveloper/easter"
homepage = ""
bugtracker = "https://github.com/alexkdeveloper/easter/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/alexkdeveloper/easter"
screenshots = [ "https://raw.githubusercontent.com/alexkdeveloper/easter/main/data/screenshot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.alexkdeveloper.easter/1.png", "https://img.linuxphoneapps.org/com.github.alexkdeveloper.easter/2.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.alexkdeveloper.easter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.alexkdeveloper.easter"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/alexkdeveloper/easter/main/data/com.github.alexkdeveloper.easter.appdata.xml.in"
reported_by = "linmob"
updated_by = ""

+++



### Notice

Very limited feature set, but it already helped me this week.
