+++
title = "Telegrand"
description = "A GTK4 telegram client built to be well integrated with the GNOME desktop environment."
aliases = []
date = 2021-04-18
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "melix99",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Telegram",]
services = [ "Telegram",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/melix99/telegrand"
homepage = ""
bugtracker = "https://github.com/melix99/telegrand/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/melix99/telegrand"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.melix99.telegrand"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "telegrand",]
appstream_xml_url = "https://raw.githubusercontent.com/melix99/telegrand/main/data/com.github.melix99.telegrand.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Notice

WIP, far from feature complete but works using own API key.