+++
title = "Liri Browser"
description = "Liri Browser is a cross-platform, Material Design Web browser, currently in  alpha  stage. "
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "lirios",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Network", "WebBrowser",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/lirios/browser"
homepage = "https://liri.io/apps/browser/"
bugtracker = "https://github.com/lirios/browser/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://liri.io/apps/browser/"
screenshots = [ "https://liri.io/apps/browser/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.liri.Browser"
scale_to_fit = "io.liri.Browser"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "liri-browser",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"
+++

