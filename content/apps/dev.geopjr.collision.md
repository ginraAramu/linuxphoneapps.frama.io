+++
title = "Collision"
description = "Check hashes for your files"
aliases = []
date = 2023-04-17

[taxonomies]
project_licenses = [ "BSD-2-Clause",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Evangelos \"GeopJr\" Paterakis",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = []
programming_languages = [ "Crystal",]
build_systems = [ "make",]

[extra]
repository = "https://github.com/GeopJr/Collision/"
homepage = "https://collision.geopjr.dev/"
bugtracker = "https://github.com/GeopJr/Collision/issues"
donations = "https://geopjr.dev/donate"
translations = "https://hosted.weblate.org/engage/collision/"
more_information = [ "https://apps.gnome.org/de/app/dev.geopjr.Collision/",]
summary_source_url = "https://raw.githubusercontent.com/GeopJr/Collision/main/data/dev.geopjr.Collision.metainfo.xml.in"
screenshots = [ "https://i.imgur.com/TMwCOWY.png", "https://i.imgur.com/pmIdbwf.png", "https://i.imgur.com/n9HNuEp.png", "https://i.imgur.com/21com71.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/dev.geopjr.collision/1.png", "https://img.linuxphoneapps.org/dev.geopjr.collision/2.png", "https://img.linuxphoneapps.org/dev.geopjr.collision/3.png", "https://img.linuxphoneapps.org/dev.geopjr.collision/4.png",]
all_features_touch = 1
intended_for_mobile = 1
app_id = "dev.geopjr.Collision"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.geopjr.Collision"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/GeopJr/Collision/main/data/dev.geopjr.Collision.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "collision",]
appstream_xml_url = "https://raw.githubusercontent.com/GeopJr/Collision/main/data/dev.geopjr.Collision.metainfo.xml.in"
reported_by = "linmob"
+++


### Description

Verifying that a file you downloaded or received is actually the one you were expecting is often overlooked or too time-consuming to do. At the same time, it has become very easy to get your hands on a file that has been tampered with, due to the mass increase of malicious webpages and other actors.

This tool aims to solve that. Collision comes with a simple & clean UI, allowing anyone, from any age and experience group, to generate, compare and verify MD5, SHA-256, SHA-512 and SHA-1 hashes. [Source](https://raw.githubusercontent.com/GeopJr/Collision/main/data/dev.geopjr.Collision.metainfo.xml.in)

