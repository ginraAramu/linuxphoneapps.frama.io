+++
title = "Store Cards"
description = "Keep all your store cards on your linux phone"
aliases = []
date = 2022-01-23
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "fdservices",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Tk",]
backends = [ "zint",]
services = []
packaged_in = []
freedesktop_categories = [ "Utility",]
programming_languages = [ "Tcl",]
build_systems = [ "none",]

[extra]
repository = "https://github.com/fdservices/storecards"
homepage = ""
bugtracker = "https://github.com/fdservices/storecards/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/fdservices/storecards"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Simple app to store membership/coupon cards - enter a number and it generates a barcode.