+++
title = "Swooper"
description = "Minesweeper for Plasma Mobile"
aliases = []
date = 2020-09-28

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "linmob"
verified = "❎"
repository = "https://invent.kde.org/luie/Swooper"
homepage = ""
more_information = []
summary_source_url = "https://invent.kde.org/luie/Swooper"
screenshots = []
screenshots_img = []
app_id = "org.kde.swooper"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/luie/Swooper/-/raw/master/org.kde.swooper.appdata.xml"

+++


### Description

This is a simple Minesweeper implementation for Plasma Mobile! [Source](https://invent.kde.org/luie/Swooper)