+++
title = "Poland can into Space"
description = "‘Poland can into Space’ is a simple Android game based on LibGDX framework"
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "NPOSL-3.0",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/JSandomierz/pcis"
homepage = ""
more_information = []
summary_source_url = "https://github.com/JSandomierz/pcis"
screenshots = [ "https://user-images.githubusercontent.com/10513420/41747762-7094deb2-75af-11e8-903c-c0c3ab9a6f43.jpg",]
screenshots_img = []
app_id = "pl.sanszo.pcis"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++