+++
title = "Ironball"
description = "Mobile game inspired by the classic Speedball game for Amiga/C64 etc."
aliases = []
date = 2019-02-16

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "Web technologies",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/pracedru/ironball"
homepage = "https://www.pracedru.dk:8888/index.html"
more_information = []
summary_source_url = "https://github.com/pracedru/ironball/"
screenshots = [ "https://github.com/pracedru/ironball/",]
screenshots_img = []
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++