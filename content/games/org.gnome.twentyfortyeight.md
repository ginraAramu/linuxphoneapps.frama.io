+++
title = "GNOME 2048"
description = "The classic 2048 puzzle game, made for GNOME"
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Debian", "Flathub",]

[extra]
reported_by = "Moxvallix"
verified = "✅"
repository = "https://gitlab.gnome.org/GNOME/gnome-2048"
homepage = "https://wiki.gnome.org/Apps/2048"
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
app_id = "org.gnome.TwentyFortyEight"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.TwentyFortyEight"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-2048",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-2048/-/raw/master/data/org.gnome.TwentyFortyEight.appdata.xml.in"

+++
