+++
title = "GzDoom"
description = "Zdoom is a family of enhanced ports of the Doom engine."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "AUR", "postmarketOS", "Flathub",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/ZDoom/gzdoom"
homepage = "https://zdoom.org/index"
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
app_id = "org.zdoom.GZDoom"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.zdoom.GZDoom"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gzdoom",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/org.zdoom.GZDoom.appdata.xml"

+++
